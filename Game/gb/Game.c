/*
 *
 * problem
 * keine ide keine docu keine tuts tiles random ist kein random
A	B button
S	A button
Shift	Select button
Enter	Start button
F2	save state
F3	select state
F4	load state
Esc	debugger
 */

#include <gb/gb.h>
#include <gb/sample.h>
#include <stdio.h>
#include <gb/drawing.h>
#include <rand.h>
#include <stdlib.h>


unsigned char numbers[] = {0x18, 0x18, 0x28, 0x28, 0x48, 0x48, 0x08, 0x08,
                           0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
                           0x38, 0x38, 0x68, 0x68, 0x48, 0x48, 0x18, 0x18,
                           0x10, 0x10, 0x30, 0x30, 0x60, 0x60, 0x7C, 0x7C,
                           0x3C, 0x3C, 0x24, 0x24, 0x04, 0x04, 0x04, 0x04,
                           0x3C, 0x3C, 0x04, 0x04, 0x24, 0x24, 0x3C, 0x3C,
                           0x10, 0x10, 0x30, 0x30, 0x20, 0x20, 0x28, 0x28,
                           0x7E, 0x7E, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
                           0x7C, 0x7C, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40,
                           0x7C, 0x7C, 0x04, 0x04, 0x04, 0x04, 0x7C, 0x7C
};
unsigned char warior[] =
        {
                0x00, 0x30, 0x02, 0x7A, 0x7A, 0x2A, 0x7A, 0x02,
                0x07, 0xFF, 0x06, 0xF8, 0x02, 0xFA, 0x48, 0x48,
                0x00, 0x30, 0x02, 0x7A, 0x7A, 0x2A, 0x7A, 0x02,
                0x07, 0xFF, 0x06, 0xF8, 0x02, 0xFA, 0x90, 0x90,
                0x00, 0x30, 0x00, 0x78, 0x7A, 0x28, 0x79, 0x00,
                0x05, 0xFC, 0x0F, 0xFF, 0x04, 0xFC, 0x48, 0x48,
                0x00, 0x00, 0x18, 0x18, 0x0C, 0x0C, 0xFE, 0xFE,
                0xFE, 0xFE, 0x0C, 0x0C, 0x18, 0x18, 0x00, 0x00,
                0x00, 0x80, 0x20, 0x20, 0x8C, 0x8C, 0x16, 0x5E,
                0x4A, 0x5E, 0x16, 0x1E, 0x2C, 0x6C, 0x80, 0x80,
                0x00, 0x00, 0x80, 0x80, 0x2C, 0x2C, 0x16, 0x1E,
                0x8A, 0xDE, 0x16, 0x1E, 0x4C, 0x5C, 0x00, 0x80,
                0x00, 0x00, 0xF0, 0xF0, 0x84, 0x84, 0xE0, 0xE0,
                0x84, 0x84, 0x84, 0x84, 0x84, 0x84, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0xA6, 0xA6, 0xC9, 0xC9,
                0x8F, 0x8F, 0x88, 0x88, 0x86, 0x86, 0x00, 0x00,
                0x00, 0x00, 0x90, 0x90, 0x94, 0x94, 0x90, 0x90,
                0xF4, 0xF4, 0x94, 0x94, 0x94, 0x94, 0x94, 0x94,
                0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0xE0, 0xE0,
                0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x60, 0x60,
                0x00, 0x1C, 0x00, 0x2A, 0x00, 0x9C, 0x00, 0x48,
                0x00, 0x3E, 0x00, 0x09, 0x00, 0x15, 0x00, 0x14,
                0x3C, 0x00, 0x7E, 0x24, 0x7E, 0x00, 0x7E, 0x3C,
                0x7E, 0x18, 0x3C, 0x80, 0x00, 0x7C, 0x28, 0x02,
                0x7C, 0x00, 0x92, 0x00, 0x92, 0x00, 0xB6, 0x00,
                0xFE, 0x00, 0xFE, 0x00, 0xFE, 0x00, 0xAA, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x08, 0x08,
                0x3E, 0x3E, 0x08, 0x08, 0x08, 0x08, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x04, 0x04, 0x1C, 0x1C, 0x34, 0x34, 0x04, 0x04,
                0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
                0x08, 0x08, 0x3C, 0x3C, 0x64, 0x64, 0x0C, 0x0C,
                0x18, 0x18, 0x20, 0x20, 0x20, 0x20, 0x3E, 0x3E,
                0x38, 0x38, 0x2C, 0x2C, 0x0C, 0x0C, 0x18, 0x18,
                0x18, 0x18, 0x08, 0x08, 0x08, 0x08, 0x38, 0x38,
                0x20, 0x20, 0x20, 0x20, 0x60, 0x60, 0x68, 0x68,
                0x3C, 0x3C, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
                0x3F, 0x3F, 0x20, 0x20, 0x60, 0x60, 0x70, 0x70,
                0x18, 0x18, 0x04, 0x04, 0x66, 0x66, 0x3E, 0x3E,
                0x18, 0x18, 0x30, 0x30, 0x60, 0x60, 0x40, 0x40,
                0x78, 0x78, 0x44, 0x44, 0x44, 0x44, 0x7C, 0x7C,
                0x7C, 0x7C, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
                0x1F, 0x1F, 0x06, 0x06, 0x04, 0x04, 0x04, 0x04,
                0x38, 0x38, 0x68, 0x68, 0x68, 0x68, 0x38, 0x38,
                0x1C, 0x1C, 0x34, 0x34, 0x2C, 0x2C, 0x38, 0x38,
                0x78, 0x78, 0x48, 0x48, 0x78, 0x78, 0x08, 0x08,
                0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x78, 0x78
        };

unsigned char number1[] = {

        0x04, 0x04, 0x1C, 0x1C, 0x34, 0x34, 0x04, 0x04,
        0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04
};
unsigned char number2[] = {

        0x08, 0x08, 0x3C, 0x3C, 0x64, 0x64, 0x0C, 0x0C,
        0x18, 0x18, 0x20, 0x20, 0x20, 0x20, 0x3E, 0x3E
};
unsigned char number3[] = {

        0x38, 0x38, 0x2C, 0x2C, 0x0C, 0x0C, 0x18, 0x18,
        0x18, 0x18, 0x08, 0x08, 0x08, 0x08, 0x38, 0x38
};

unsigned char number4[] = {

        0x20, 0x20, 0x20, 0x20, 0x60, 0x60, 0x68, 0x68,
        0x3C, 0x3C, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08
};

unsigned char number5[] = {

        0x3F, 0x3F, 0x20, 0x20, 0x60, 0x60, 0x70, 0x70,
        0x18, 0x18, 0x04, 0x04, 0x66, 0x66, 0x3E, 0x3E
};

unsigned char number6[] = {

        0x18, 0x18, 0x30, 0x30, 0x60, 0x60, 0x40, 0x40,
        0x78, 0x78, 0x44, 0x44, 0x44, 0x44, 0x7C, 0x7C
};

unsigned char number7[] = {
        0x7C, 0x7C, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
        0x1F, 0x1F, 0x06, 0x06, 0x04, 0x04, 0x04, 0x04
};

unsigned char number8[] = {

        0x38, 0x38, 0x68, 0x68, 0x68, 0x68, 0x38, 0x38,
        0x1C, 0x1C, 0x34, 0x34, 0x2C, 0x2C, 0x38, 0x38,

};

unsigned char number9[] = {
        0x78, 0x78, 0x48, 0x48, 0x78, 0x78, 0x08, 0x08,
        0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x78, 0x78
};

unsigned char number0[] = {
        0x3C, 0x3C, 0x66, 0x66, 0x42, 0x42, 0x42, 0x42,
        0x42, 0x42, 0x66, 0x66, 0x34, 0x34, 0x1C, 0x1C
};


unsigned char numberSlash[] = {
        0x01, 0x01, 0x03, 0x03, 0x06, 0x06, 0x0C, 0x0C,
        0x18, 0x18, 0x30, 0x30, 0x60, 0x60, 0xC0, 0xC0
};

//prototype
int createRanInt(int start, int end);

typedef struct PlayerStruct {

    int xPos, yPos, hp, hpMax;
    int intell, dex, str;
    int level, exp;

} PlayerStruct;


typedef struct MonsterStruct {
    int hp, hpMax, monstertile;
} MonsterStruct;


void redrawBG() {

    int xmim;
    int ymim;
    UBYTE i, j;
    int imim;

    //unsigned char tiles[500];
    int ran;

    unsigned char space[] =
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    unsigned char bush[] =
            {0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x12, 0x00,
             0xD4, 0x00, 0x58, 0x00, 0x38, 0x00, 0x00, 0x00};

    unsigned char stone[] =
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00,
             0x38, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00};

    unsigned char bkg_tiles[] = {
            0x00, 0x01, 0x02, 0x03, 0xFC, 0xFC, 0x04, 0xFC,
            0xFC, 0x05, 0x06, 0xFC, 0x07, 0x08, 0x09, 0x0A,
            0xFC, 0xFC, 0xFC, 0x02, 0x0B, 0x0C, 0x0D, 0xFC,
            0x0E, 0x0F, 0x10, 0xFC, 0x11, 0x12, 0x13, 0x14,
            0x15, 0x16, 0x17, 0xFC, 0x18, 0x19, 0x1A, 0xFC,
            0x1B, 0x1C, 0x1D, 0xFC, 0xFC, 0x1E, 0x1F, 0x20,
            0x21, 0x22, 0xFC, 0x23, 0x24, 0x25, 0xFC, 0x26,
            0x27, 0x13, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x11
    };

    unsigned char kaktus[] =
            {
                    0x00, 0x00, 0x10, 0x00, 0x14, 0x00, 0x5C, 0x00,
                    0x70, 0x00, 0x10, 0x00, 0x38, 0x00, 0x00, 0x00
            };

    set_bkg_data(5, 1, kaktus);
    set_bkg_data(7, 1, stone);
    set_bkg_data(20, 1, bush);
    set_bkg_data(22, 1, stone);
    set_bkg_data(25, 1, kaktus);
    set_bkg_data(33, 1, bush);

    for (i = 0; i < 32; i += 8)
        for (j = 0; j < 32; j += 8)
            set_bkg_tiles(i, j, 8, 8, bkg_tiles);


}

void drawBG() {

    int xmim;
    int ymim;
    UBYTE i, j;
    int imim;

    //unsigned char tiles[500];
    int ran;

    /*
    unsigned char space[] =
            { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
              0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 };
    */
    unsigned char bush[] =
            {0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x12, 0x00,
             0xD4, 0x00, 0x58, 0x00, 0x38, 0x00, 0x00, 0x00};

    unsigned char stone[] =
            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00,
             0x38, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00};

    unsigned char kaktus[] =
            {
                    0x00, 0x00, 0x10, 0x00, 0x14, 0x00, 0x5C, 0x00,
                    0x70, 0x00, 0x10, 0x00, 0x38, 0x00, 0x00, 0x00
            };


    unsigned char bkg_tiles[] = {
            0x00, 0x01, 0x02, 0x03, 0xFC, 0xFC, 0x04, 0xFC,
            0xFC, 0x05, 0x06, 0xFC, 0x07, 0x08, 0x09, 0x0A,
            0xFC, 0xFC, 0xFC, 0x02, 0x0B, 0x0C, 0x0D, 0xFC,
            0x0E, 0x0F, 0x10, 0xFC, 0x11, 0x12, 0x13, 0x14,
            0x15, 0x16, 0x17, 0xFC, 0x18, 0x19, 0x1A, 0xFC,
            0x1B, 0x1C, 0x1D, 0xFC, 0xFC, 0x1E, 0x1F, 0x20,
            0x21, 0x22, 0xFC, 0x23, 0x24, 0x25, 0xFC, 0x26,
            0x27, 0x13, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x11
    };

    /*

    //set_bkg_data(UBYTE first_tile, UBYTE nb_tiles, unsigned char *data);
    //first_tile: number of the first tile in VRAM (between 0 and 255).
    //nb_tiles: number of tiles to copy (0 for copying 256 tiles).
    //data: pointer to the user tile data.
    //set_bkg_data(0, 17, tiles);


    //set_bkg_tiles(0, 0, 11, 13, tiles);
    // set_bkg_tiles(UBYTE x,UBYTE y,UBYTE w,UBYTE h, unsigned char *tiles);
    /*  x: horizontal coordinate of the top-left corner of the rectangular area in VRAM.
        y: vertical coordinate of the top-left corner of the rectangular area in VRAM.
        w: width of the the rectangular area in VRAM.
        h: height of the the rectangular area in VRAM.
        tiles: pointer to the user tile numbers. The size of the tile numbers should be w*h.
        */
    //for(i = 0; i < 19; i++)
    //    set_bkg_data(i, 1, space);

    set_bkg_data(5, 1, kaktus);
    set_bkg_data(7, 1, stone);
    set_bkg_data(20, 1, bush);
    set_bkg_data(22, 1, stone);
    set_bkg_data(25, 1, kaktus);
    set_bkg_data(33, 1, bush);

    for (i = 0; i < 32; i += 8)
        for (j = 0; j < 32; j += 8)
            set_bkg_tiles(i, j, 8, 8, bkg_tiles);
    /*
i = 0;

for (x = 0; x < 3 ; x+=4) {
    for (y = 0; y < 3; y += 4) {

        ran = createRanInt(0, 3);
        switch (ran) {
            case 0: //space
                //set_bkg_tiles(x, y, 4, 4, space);
                tiles[i] = 0x00;
                tiles[i + 1] = 0x00;
                tiles[i + 2] = 0x00;
                tiles[i + 3] = 0x00;
                tiles[i + 4] = 0x00;
                tiles[i + 5] = 0x00;
                tiles[i + 6] = 0x00;
                tiles[i + 7] = 0x00;
                tiles[i + 8] = 0x00;
                tiles[i + 9] = 0x00;
                tiles[i + 10] = 0x00;
                tiles[i + 11] = 0x00;
                tiles[i + 12] = 0x00;
                tiles[i + 13] = 0x00;
                tiles[i + 14] = 0x00;
                tiles[i + 15] = 0x00;
                i += 15;
                break;
            case 1: //bush
                // set_bkg_tiles(x, y, 4, 4, bush);
                tiles[i] = 0x00;
                tiles[i + 1] = 0x00;
                tiles[i + 2] = 0x00;
                tiles[i + 3] = 0x00;
                tiles[i + 4] = 0x20;
                tiles[i + 5] = 0x00;
                tiles[i + 6] = 0x12;
                tiles[i + 7] = 0x00;
                tiles[i + 8] = 0xD4;
                tiles[i + 9] = 0x00;
                tiles[i + 10] = 0x58;
                tiles[i + 11] = 0x00;
                tiles[i + 12] = 0x30;
                tiles[i + 13] = 0x08;
                tiles[i + 14] = 0x00;
                tiles[i + 15] = 0x00;
                i += 15;
                break;
            case 2: //bush
                //set_bkg_tiles(x, y, 4, 4, stone);
                tiles[i] = 0x00;
                tiles[i + 1] = 0x00;
                tiles[i + 2] = 0x00;
                tiles[i + 3] = 0x00;
                tiles[i + 4] = 0x20;
                tiles[i + 5] = 0x00;
                tiles[i + 6] = 0x12;
                tiles[i + 7] = 0x00;
                tiles[i + 8] = 0xD4;
                tiles[i + 9] = 0x00;
                tiles[i + 10] = 0x58;
                tiles[i + 11] = 0x00;
                tiles[i + 12] = 0x30;
                tiles[i + 13] = 0x08;
                tiles[i + 14] = 0x00;
                tiles[i + 15] = 0x00;
                i += 15;
                break;
        }
    }
}



    SHOW_BKG;
    DISPLAY_ON;
    */
    SHOW_BKG;
    // DISPLAY_ON;

}

int createRanInt(int start, int end) {
    int random;
    srand(time(NULL));

    /* erzeugt Zufallszahlen zwischen start und end */
    random = rand() % end;
    return random;
}

/*drawLifePoint("987654321"); */
void drawLifePoint(char text[10]) {
//void drawLifePoint(int hp) {

    //set_sprite_data(0, 16, numbers); //load from 0 to 9 tiles

    int Romtile = 10;
    //char text[10];
    int xPos = 30;
    int yPos = 20;
    char str[10];
    char str2[10];
    //int start_tile = 12;
    int i = 1;
    int tile_number = 16;

    /*
    printf(str, "%d", hp);
    sprintf(str2, "%d", "99");
    strcpy(text, str);
    strcat(text, "/");
    strcat(text, str2);
    */

    for (i = 0; text[i] != '\0'; i++) {

        char letter = text[i];
        tile_number++;
        Romtile++;
        switch (letter) {
            case '1':
                set_sprite_data(Romtile, Romtile, number1);
                set_sprite_tile(tile_number, Romtile);
                break;
            case '2':
                set_sprite_data(Romtile, Romtile, number2);
                set_sprite_tile(tile_number, Romtile);

                break;
            case '3':
                set_sprite_data(Romtile, Romtile, number3);
                set_sprite_tile(tile_number, Romtile);

                break;
            case '4':
                set_sprite_data(Romtile, Romtile, number4);
                set_sprite_tile(tile_number, Romtile);

                break;
            case '5':
                set_sprite_data(Romtile, Romtile, number5);
                set_sprite_tile(tile_number, Romtile);

                break;
            case '6':
                set_sprite_data(Romtile, Romtile, number6);
                set_sprite_tile(tile_number, Romtile);

                break;
            case '7':
                set_sprite_data(Romtile, Romtile, number7);
                set_sprite_tile(tile_number, Romtile);
                break;
            case '8':
                set_sprite_data(Romtile, Romtile, number8);
                set_sprite_tile(tile_number, Romtile);
                break;
            case '9':
                set_sprite_data(Romtile, Romtile, number9);
                set_sprite_tile(tile_number, Romtile);
                break;
            case '0':
                set_sprite_data(Romtile, Romtile, number0);
                set_sprite_tile(tile_number, Romtile);
                break;
            case '/':
                set_sprite_data(Romtile, Romtile, numberSlash);
                set_sprite_tile(tile_number, Romtile);
                break;
            default :
                2 + 4;

        }
        move_sprite(tile_number, xPos + (8 * i), yPos);
    }
    //set_sprite_data(0, 16, warior);
    move_sprite(6, 77, 77);
}


void clearLifeBar() {
    int x = 0;
    int y = 0;
    // box(0, 10, 110, 20, M_NOFILL);

    for (y = 10; y < 21; y++) {
        for (x = 0; x < 111; x++) {
            plot(x, y, 0);
        }
    }

    //box(60, 90, 159, 100, M_NOFILL);
    for (y = 89; y < 101; y++) {
        for (x = 58; x < 161; x++) {
            plot(x, y, 0);
        }
    }
}

void clearFightMenu() {

//    box(20, 110, 140, 140, M_NOFILL);
    int x = 0;
    int y = 0;

    //clear exp bar
    for (y = 21; y < 25; y++) {
        for (x = 20; x < 75; x++) {
            plot(x, y, 0);
        }
    }

    //clear fight box
    for (y = 110; y < 141; y++) {
        for (x = 20; x < 141; x++) {
            plot(x, y, 0);
        }
    }

    //clear hit and fire tiles
    move_sprite(6, 200, 200); // move sprite 0 to x and y coords
    move_sprite(7, 200, 200); // move sprite 0 to x and y coords
    move_sprite(8, 200, 200); // move sprite 0 to x and y coords
    move_sprite(9, 200, 200); // move sprite 0 to x and y coords
    move_sprite(10, 200, 200); // move sprite 0 to x and y coords
    move_sprite(11, 200, 200); // move sprite 0 to x and y coords
    move_sprite(12, 200, 200); // move sprite 0 to x and y coords
    move_sprite(13, 200, 200); // move sprite 0 to x and y coords
    move_sprite(14, 200, 200); // move sprite 0 to x and y coords
    move_sprite(15, 200, 200); // move sprite 0 to x and y coords


}

void drawFightMenu(int player_hp, int player_hpmax, int monster_hp, int monster_hpmax, int player_exp) {

    int a = 0;
    char lifebar[10];
    char str[10];
    char str2[10];
    int hp = player_hp;

    //lifebar = {"100/100"};
    /*
    sprintf(str, "%d", hp);
    sprintf(str2, "%d", "100");
    strcpy(lifebar, str);
    strcat(lifebar, "/");
    strcat(lifebar, str2);
    `*/
    //drawLifePoint(hp);
    //drawLifePoint("99/99");

    clearLifeBar();
    //clear screen
    //draw life bar
    //color(LTGREY, WHITE, SOLID);
    //clear screen


    color(BLACK, WHITE, SOLID);
    box(0, 10, 110, 20, M_NOFILL);

    color(BLACK, LTGREY, SOLID);
    box(0, 10, (player_hp * 100 / player_hpmax), 20, M_FILL);
    //wtf!!!!!
    //erst weis dann grün und dann grün und dann weis.....

    //monster hp
    color(BLACK, LTGREY, SOLID);
    box(158 - (monster_hp * 100 / monster_hpmax), 90, 159, 100, M_FILL);
    color(BLACK, WHITE, SOLID);
    box(58, 90, 159, 100, M_NOFILL);


    //draw monster
    move_sprite(1, 120, 70); // move sprite 0 to x and y coords

    //figtmenu
    box(20, 110, 140, 140, M_NOFILL);

    //feuer anzeigen
    move_sprite(6, 60, 130); // move sprite 0 to x and y coords
    move_sprite(7, 69, 130); // move sprite 0 to x and y coords


    //hit anzeigen
    move_sprite(8, 110, 130); // move sprite 0 to x and y coords
    move_sprite(9, 119, 130); // move sprite 0 to x and y coords

    //feuer +
    set_sprite_tile(10, 6); //set from 0 to 0 as aktive tile
    set_sprite_tile(11, 7); //set from 0 to 0 as aktive tile
    set_sprite_tile(12, 13); //set from 0 to 0 as aktive tile


    //hit +
    set_sprite_tile(13, 8); //set from 0 to 0 as aktive tile
    set_sprite_tile(14, 9); //set from 0 to 0 as aktive tile
    set_sprite_tile(15, 13); //set from 0 to 0 as aktive tile



    //feuer anzeigen
    move_sprite(10, 60, 140); // move sprite 0 to x and y coords
    move_sprite(11, 69, 140); // move sprite 0 to x and y coords
    move_sprite(12, 77, 140); // move sprite 0 to x and y coords


    //hit anzeigen
    move_sprite(13, 110, 140); // move sprite 0 to x and y coords
    move_sprite(14, 119, 140); // move sprite 0 to x and y coords
    move_sprite(15, 127, 140); // move sprite 0 to x and y coords

    //clear screen
    color(BLACK, 0, SOLID);
    box(0, 155, 0, 155, M_FILL);

    //exp bar
    box(20, 20, 71, 23, M_NOFILL);

    for (a = 0; a < player_exp; a++) {
        plot(21 + a, 22, 1);
    }
}

void attack(char attacke) {

    int a = 40;
    switch (attacke) {
        case 'f':
            //feuerball
            //krieger
            move_sprite(0, 30, 70); // move sprite 0 to x and y coords

            for (a = 40; a < 110; a = a + 6) {

                move_sprite(4, a, 70); // move sprite 0 to x and y coords
                delay(150);
                move_sprite(4, 200, 200); // move sprite 0 to x and y coords
                move_sprite(5, a, 70); // move sprite 0 to x and y coords
                delay(150);
                move_sprite(5, 200, 200); // move sprite 0 to x and y coords
            }
            break;

        case 'g':
            //feuerball +
            move_sprite(0, 30, 70); // move sprite 0 to x and y coords
            for (a = 40; a < 110; a = a + 18) {
                move_sprite(4, a, 70); // move sprite 0 to x and y coords
                delay(150);
                move_sprite(4, 200, 200); // move sprite 0 to x and y coords
                move_sprite(5, a, 70); // move sprite 0 to x and y coords
                delay(150);
                move_sprite(5, 200, 200); // move sprite 0 to x and y coords
            }
            break;
        case 's' :

            //slash
            //move to opponent
            a = 40;
            for (a = 40; a < 110; a = a + 6) {
                delay(150);
                set_sprite_tile(0, 1);
                move_sprite(0, a, 70);
                delay(150);
                set_sprite_tile(0, 0);
            }

            //slash
            delay(150);
            set_sprite_tile(0, 2);
            delay(150);
            set_sprite_tile(0, 0);
            delay(150);
            set_sprite_tile(0, 2);
            delay(150);
            set_sprite_tile(0, 0);
            move_sprite(0, 30, 70); // move sprite 0 to x and y coords
            break;
        case 'd' :

            //slash +
            //move to opponent
            a = 40;
            for (a = 40; a < 110; a = a + 12) {
                delay(150);
                set_sprite_tile(0, 1);
                move_sprite(0, a, 70);
                delay(150);
                set_sprite_tile(0, 0);
            }

            //slash
            delay(50);
            set_sprite_tile(0, 2);
            delay(50);
            set_sprite_tile(0, 0);
            delay(50);
            set_sprite_tile(0, 2);
            delay(50);
            set_sprite_tile(0, 0);
            set_sprite_tile(0, 2);
            delay(50);
            set_sprite_tile(0, 0);
            delay(50);
            set_sprite_tile(0, 2);
            delay(50);
            set_sprite_tile(0, 0);
            delay(50);
            set_sprite_tile(0, 2);
            delay(50);
            set_sprite_tile(0, 0);
            delay(50);

            move_sprite(0, 30, 70);
            break;
    }

}

void walk(int x, int y) {
    move_sprite(0, x, y); // move sprite 0 to x and y coords
    set_sprite_tile(0, 0);
    delay(150);
    set_sprite_tile(0, 1); // move sprite 0 to x and y coords
    delay(150);
}

void mainGame() {

    int curserPosY = 0; //ingame curser position
    int curserPosX = 0;

    int x = 40; // Our beginning x coord
    int y = 80; // Our beginning y coord
    int worldX = 0; // Our beginning world x coord
    int worldY = 0; // Our beginning world y coord

    int fight = 0; //toogle; are we in a fight? 1 == true
    int fightInit = 1;
    int r = 0;

    int figthProbal = 10; //probabilyti to encounter a fight 1:10
    /*
    int exp = 7;
    int hp = 100;
    int level = 1;
    */

    char direction = 's'; //used for viewpoint  values nswe
    char attacke = 'f'; //name of the attacke fireball f or s for slash


    //struct PlayerStruct player;
    //gbc mag keine structs....
    int player_hp = 100;
    int player_hpmax = 100;
    int player_xPos = 40;
    int player_yPos = 80;
    int player_level = 1;
    int player_exp = 0;
    int player_int = 0;
    int player_dex = 0;

    //MonsterStruct Monster;
    int monster_hp = 100;
    int monster_hpmax = 100;
    int monster_monstertile = 0;


    //create background and draw it
    drawBG();

    //krieger
    SPRITES_8x8;
    set_sprite_data(0, 16, warior); //load from 0 to 9 tiles
    set_sprite_tile(0, 0);
    set_sprite_tile(1, 1);
    set_sprite_tile(2, 2);

    //pfeil
    set_sprite_tile(3, 3); //set from 0 to 0 as aktive tile

    //feuerball
    set_sprite_tile(4, 4); //set from 0 to 0 as aktive tile
    set_sprite_tile(5, 5); //set from 0 to 0 as aktive tile

    //fire text
    set_sprite_tile(6, 6); //set from 0 to 0 as aktive tile
    set_sprite_tile(7, 7); //set from 0 to 0 as aktive tile

    //hittext
    set_sprite_tile(8, 8); //set from 0 to 0 as aktive tile
    set_sprite_tile(9, 9); //set from 0 to 0 as aktive tile


    //krieger init
    move_sprite(0, x, y); // move sprite 0 to x and y coords
    SHOW_SPRITES;

    while (1) {

        if (fight == 1) {

            if (fight == 1 && fightInit == 1) {
                //init

                //gegner
                //init fight krieger hinstellen
                move_sprite(0, 30, 70);

                monster_monstertile = createRanInt(0, 3);

                switch (monster_monstertile) {
                    case 0:
                        set_sprite_tile(1, 10); //set from 0 to 0 as aktive tile
                        monster_hpmax = 100 + 10 * (worldX + worldY);
                        break;
                    case 1:
                        set_sprite_tile(1, 11); //set from 0 to 0 as aktive tile
                        monster_hpmax = 40 + 10 * (worldX + worldY);
                        break;
                    case 2:
                        set_sprite_tile(1, 12); //set from 0 to 0 as aktive tile
                        monster_hpmax = 70 + 10 * (worldX + worldY);
                        break;
                    default:
                        set_sprite_tile(1, 12); //set from 0 to 0 as aktive tile
                }
                monster_hp = monster_hpmax;

                //drawFightMenu(&player , &player);
                drawFightMenu(player_hp, player_hpmax, monster_hp, monster_hpmax, player_exp);
                fightInit = 0;
            }


            //moce curser up
            if (joypad() == J_UP)  // If UP is pressed
            {
                curserPosY = 0;
                /*if (curserPosY > 1) {
                    curserPosY--;
                }
                 */
            }

            if (joypad() == J_DOWN)  // If DOWN is pressed
            {
                curserPosY = 1;
                /*
                if (curserPosY < 3) {
                    curserPosY++;
                }
                */
            }


            //moce curser up
            if (joypad() == J_LEFT)  // If UP is pressed
            {
                curserPosX = 0;
                /*
                if (curserPosX > 1) {
                    curserPosX--;
                }
                 */
            }

            if (joypad() == J_RIGHT)  // If DOWN is pressed
            {
                curserPosX = 1;
                /*
                if (curserPosX < 2) {
                    curserPosX++;
                }
                 */
            }

            //move courser
            move_sprite(3, curserPosX * 50 + 50, 130 + curserPosY * 10);
            delay(100);

            //Attack gedrückt
            if (joypad() == J_A) {
                if (curserPosX == 0 && curserPosY == 0) {
                    attacke = 'f'; //fireball
                    monster_hp -= (20 + (2 * player_int));
                }

                if (curserPosX == 0 && curserPosY == 1) {
                    attacke = 'g'; //fireball +
                    monster_hp -= ((10 + 10 * player_int));
                }

                if (curserPosX == 1 && curserPosY == 1) {
                    attacke = 's'; //slash +
                    monster_hp -= (100 + player_dex * 2);
                }
                if (curserPosX == 1 && curserPosY == 0) {
                    attacke = 'd'; //slash
                    monster_hp -= (70 + player_dex * 10);
                }

                if (monster_hp <= 0) {
                    monster_hp = 0;
                }


                //drawFightMenu(&player , &player);
                drawFightMenu(player_hp, player_hpmax, monster_hp, monster_hpmax, player_exp);
                attack(attacke);

                //Sieg win
                if (monster_hp < 1) {
                    //Pfeile entfernen monster entfernen und char wieder setzten
                    move_sprite(3, 180, 180);
                    move_sprite(1, 180, 180);
                    move_sprite(0, x, y);

                    clearLifeBar();
                    clearFightMenu();
                    //redrawBG();
                    //SHOW_SPRITES;
                    player_exp += 10 + (worldX + worldY);

                    //leveln
                    while (player_exp > 49) {
                        player_exp -= 50;

                        player_level++;
                        player_dex++;
                        player_int++;
                        player_hpmax += 10;
                        player_hp = player_hpmax;

                    }

                    //won
                    fight = 0;
                    fightInit = 1;

                } else {

                    //calc damag taken
                    switch (monster_monstertile) {
                        case 0:
                            player_hp -= 10 + (worldX + worldY);
                            break;
                        case 1:
                            player_hp -= 20 + (worldX + worldY);
                            break;
                        case 2:
                            player_hp -= 10 + (3 * (worldX + worldY));
                            break;
                    }
                }
            }
            //move around
        } else {
            //aktion
            if (joypad() == J_A) {
                action(x, y, direction); //last direction
            }


            if (joypad() == J_RIGHT) // If RIGHT is pressed
            {
                fight = createRanInt(0, figthProbal);
                fight = 1;
                if (x < 160) {
                    x = x + 8;
                } else {
                    x = 8;
                    worldX += 1;
                }

                walk(x, y);
            }

            if (joypad() == J_LEFT)  // If LEFT is pressed
            {
                fight = createRanInt(0, figthProbal);
                if (x > 16) {
                    x = x - 8;
                } else {
                    x = 160;
                    worldX -= 1;
                }
                walk(x, y);
            }


            if (joypad() == J_UP)  // If UP is pressed
            {
                fight = createRanInt(0, figthProbal);
                if (y > 24) {
                    y = y - 8;
                } else {
                    y = 152;
                    worldY -= 1;
                }
                walk(x, y);
            }

            if (joypad() == J_DOWN)  // If DOWN is pressed
            {
                fight = createRanInt(0, figthProbal);
                if (y < 152) { y = y + 8; }
                else {
                    y = 16;
                    worldY += 1;
                }
                walk(x, y);
            }
        }
    }
}

void main(void) {
    mainGame();
}