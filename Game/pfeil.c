/*

 PFEIL.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 25 to 26

  Palette colors       : None.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v2.2

*/

/* Start of tile array. */
unsigned char warior[] =
{
  0x3C,0x3C,0x66,0x66,0x42,0x42,0x42,0x42,
  0x42,0x42,0x66,0x66,0x34,0x34,0x1C,0x1C,
  0x01,0x01,0x03,0x03,0x06,0x06,0x0C,0x0C,
  0x18,0x18,0x30,0x30,0x60,0x60,0xC0,0xC0
};

/* End of PFEIL.C */
