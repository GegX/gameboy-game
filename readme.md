How to get started

The game is located under
.\Game\gb\Game.gb

Source code can be found under
.\Game\gb\Game.c

to compile code use
..\..\gbdk-2.95-3-win32\bin\lcc -Wa-l -Wl-m -Wl-j -w -DUSE_SFR_FOR_REG -c -o Game.o Game.c
..\..\gbdk-2.95-3-win32\bin\lcc -Wa-//l -Wl-m -Wl-j -w -DUSE_SFR_FOR_REG -o Game.gb Game.o

to run game open the Game.gb file with the GB Emulator
[.\bgb - emulator\bgb.exe]

1. Start emulator
2. Drag and drop gb file into emulator window or right click into emulator and open ROM

to play the game

use arrow keys to move arround
A Button on Keyboard = a on GameBoy
S Button on Keyboard= b on GameBoy